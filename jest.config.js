module.exports = {
  coverageDirectory: 'coverage',
  moduleDirectories: ['node_modules', 'src', __dirname],
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
  moduleNameMapper: {
    '@(tests/.*|helpers/.*|components/.*|pages.*)': '<rootDir>/src/$1',
  },
  setupFilesAfterEnv: ['<rootDir>/src/tests/setup.ts'],
};
