import { FC } from 'react';
import { AppProps } from 'next/app';
import '@styles/global.css';

export const MyApp: FC<AppProps> = ({ Component, pageProps }) => (
  <Component {...pageProps} />
);

export default MyApp;
