import Head from 'next/head';

export const IndexPage = () => {
  <div className="container">
    <Head>
      <title>NextJS starter</title>
      <link rel="icon" href="/favicon.ico" />
    </Head>
    <main>Index page</main>
  </div>;
};

export default IndexPage;
